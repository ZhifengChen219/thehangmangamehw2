package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    VBox              figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman
    HangmanController controller;
    VBox              guessesBox;
    Group             group;
    Button[]            buttons;
    HBox              h1;
    HBox              h2;
    HBox              h3;
    HBox              h4;
    HBox              h5;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new VBox();
        figurePane.setPadding(new Insets(20, 50, 20, 20));

        group = new Group();
        Line line1 = new Line(20, 373.3, 300, 373.3);
        line1.setStrokeWidth(5);
        Line line2 = new Line(100, 373.3, 100, 20);
        line2.setStrokeWidth(2);
        Line line3 = new Line(100, 20, 200 ,20);
        line3.setStrokeWidth(2);
        Line line4 = new Line(200, 20, 200, 50);
        line4.setStrokeWidth(2);

        Circle c5 = new Circle(200, 73, 20);
        c5.setStroke(Color.BLACK);
        c5.setFill(null);
        c5.setStrokeWidth(7);

        Line line6 = new Line(200, 93, 200, 130);
        line6.setStrokeWidth(5);
        Line line7 = new Line(200, 93, 180, 120);
        line7.setStrokeWidth(5);
        Line line8 = new Line(200, 93, 220, 120);
        line8.setStrokeWidth(5);
        Line line9 = new Line(200, 130, 180, 157);
        line9.setStrokeWidth(5);
        Line line10 = new Line(200, 130, 220, 157);
        line10.setStrokeWidth(5);

        group.getChildren().addAll(line1, line2, line3, line4, c5, line6, line7, line8, line9, line10);
        initHangmanGraphic();
        figurePane.getChildren().addAll(group);

        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();

        guessesBox = new VBox();
        createButtons();
        guessesBox.getChildren().addAll(h1, h2, h3, h4, h5);

        gameTextsPane = new VBox();
        gameTextsPane.setSpacing(5);
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, guessesBox);
        bodyPane = new HBox();
        bodyPane.setSpacing(20);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }


    public Button getStartGame() {
        return startGame;
    }


    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        guessesBox = new VBox();
        createButtons();
        guessesBox.getChildren().addAll(h1, h2, h3, h4, h5);
        guessesBox.setSpacing(2);
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, guessesBox);
        gameTextsPane.setSpacing(5);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
    }

    public void setHangmanGraphic(int i) {
        Node node = group.getChildren().get(i);
        if(node instanceof Line){
            ((Line) node).setStroke(Color.BLACK);
        } else if(node instanceof Circle){
            ((Circle) node).setStroke(Color.BLACK);
        }
    }

    public void initHangmanGraphic() {
        for(Node node:group.getChildren()) {
            if(node instanceof Line){
                ((Line) node).setStroke(Color.TRANSPARENT);
            } else if(node instanceof Circle){
                ((Circle) node).setStroke(Color.TRANSPARENT);
            }
        }
    }

    private void createButtons() {
        buttons = new Button[26];
        h1 = new HBox();
        buttons[0] = new Button("a");
        buttons[1] = new Button("b");
        buttons[2] = new Button("c");
        buttons[3] = new Button("d");
        buttons[4] = new Button("e");
        buttons[5] = new Button("f");
        h1.getChildren().addAll(buttons[0], buttons[1], buttons[2], buttons[3], buttons[4], buttons[5]);
        h2 = new HBox();
        buttons[6] = new Button("g");
        buttons[7] = new Button("h");
        buttons[8] = new Button("i");
        buttons[9] = new Button("j");
        buttons[10] = new Button("k");
        buttons[11] = new Button("l");
        h2.getChildren().addAll(buttons[6], buttons[7], buttons[8], buttons[9], buttons[10], buttons[11]);
        h3 = new HBox();
        buttons[12] = new Button("m");
        buttons[13] = new Button("n");
        buttons[14] = new Button("o");
        buttons[15] = new Button("p");
        buttons[16] = new Button("q");
        buttons[17] = new Button("r");
        h3.getChildren().addAll(buttons[12], buttons[13], buttons[14], buttons[15], buttons[16], buttons[17]);
        h4 = new HBox();
        buttons[18] = new Button("s");
        buttons[19] = new Button("t");
        buttons[20] = new Button("u");
        buttons[21] = new Button("v");
        buttons[22] = new Button("w");
        buttons[23] = new Button("x");
        h4.getChildren().addAll(buttons[18], buttons[19], buttons[20], buttons[21], buttons[22], buttons[23]);
        h5 = new HBox();
        buttons[24] = new Button("y");
        buttons[25] = new Button("z");
        h5.getChildren().addAll(buttons[24], buttons[25]);
        for(int i = 0; i < buttons.length; i++) {
            buttons[i].setDisable(true);
            buttons[i].setPrefSize(55, 55);
            buttons[i].setVisible(false);
            buttons[i].setStyle("-fx-background-color: #E7F8F9; ");
        }
        h1.setSpacing(2);
        h2.setSpacing(2);
        h3.setSpacing(2);
        h4.setSpacing(2);
        h5.setSpacing(2);
    }

    public void setLetterButtonsVisibile() {
        for(int i = 0; i < buttons.length; i++) {
            buttons[i].setVisible(true);
        }
    }

    public void setLetterButtonsColor(char guess) {
        for(int i = 0; i < buttons.length; i++) {
            String str = String.valueOf(guess);
            if(str.equals(buttons[i].getText())) {
                buttons[i].setStyle("-fx-background-color: #FF6666; ");
            }
        }
    }
}
